 // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '877360689089653',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
   FB.getLoginStatus(function (response) {
      if (response.status === "connected") {
        console.log("You are logged in.");
        render(true);
        
      }
      else if (response.status === "not_authorized") {
        console.log("You are not logged in.");
        render(false);
        
      }
      else {
        console.log("You are not logged in yet.");
        render(false);
        
      }
    })
  };


  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login
      $('nav').html(
        '<nav class="navbar navbar-default" role="navigation">' +
          '<div class="container-fluid">' +
           
           '<div class="navbar-header">' +
              '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">' +
                '<span class="sr-only">Toggle navigation</span>' +
                '<span class="icon-bar"></span>' +
                '<span class="icon-bar"></span>' +
               '<span class="icon-bar"></span>' +
              '</button>' +
              '<a class="navbar-brand" href="#">IQBAL</a>' +
            '</div>' +

            '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">' +
              '<ul class="nav navbar-nav">' +
                '<button type="button" class="btn btn-danger navbar-btn logout" onclick="facebookLogout()">Logout</button>' +
              '</ul>' +
              
            '</div>' +
          '</div>' +
        '</nav>'
      )
      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="container style="padding-top: 0px"">' +
            '<div class="row">' +
              '<div class="col-sm-8">'+
               // '<div class="card">'+
                  '<div style="margin-top: 30px;" class="cover">' + '<img src="'+ user.cover.source +'" width="851px"/>' + '</div>' +
                  //'<div class="picture">' + '<img src="' + user.picture.data.url + '"/>' + '</div>' +
              '</div>' +
                  //'<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
                  '<div class="col-sm-8">'+
                    //'<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                    '<div style="padding-top: 30px; padding-bottom: 20px;" class="media">' +
                      '<div class="pull-left">' +
                        '<img style="width: 168px; height: 168px;" class="media-object" src="' + user.picture.data.url + '" alt="Photo">' +
                      '</div>' +
                      '<div class="media-body">' +
                        '<h1 class="media-heading">' + user.name + '</h1>' +
                          '<h4>' + user.about + '</h4>' +
                          '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
                      '</div>' +
                    '</div>' +
                    // '<h1 class="name">' + user.name + '</h1>' +
                    // '<h4>' + user.about + '</h4>' +
                    // '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
                    '<input id="postInput" type="text" class="form-control input-lg post" style="margin-top: 10px; width:500px;" placeholder="Ketik Status Anda"/>' +
                
                    '<button type="button" class="btn btn-primary postStatus" style="margin-top: 10px; margin-bottom: 20px;" onclick="postStatus()">Post ke Facebook</button>' +
                
                  '</div>' +
                    
              '</div>' +
          '</div>' +
          '<div class="container">' +
            '<div class="col-md-4 data">' +
            //  '<button type="button" class="btn btn-danger logout" style="margin-top: 10px; margin-bottom: 30px; margin-left: 0px;" onclick="facebookLogout()">Logout</button>' +
            '</div>' +
          '</div>'
        //   '<div class="container" style="padding-top: 0px">'+
        //   '<div class="row">'+
        //     '<div class="col-sm-8">'+
        //       '<div class="card">'+
        //         '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
        //         '<img class="picture img-circle" src="' + user.picture.data.url + '" alt="profpic" />' +
        //         '<h1>'+user.name+'</h1>'+
        //         '<h3>'+user.about+'</h3>'+
        //         '<h3>'+ user.email + ' - ' + user.gender + '</h3>'+
        //         '<button class="logout btn btn-warning" onclick="facebookLogout()">Logout</button>'+
        //       '</div>'+
        //     '</div>'+
        //   '</div>'+
        // '</div>'+
        // '<div class="container" style="padding-top: 30px; padding-bottom:40px">'+
        //   '<div class="row">'+
        //     '<div class="col-sm-8">'+
        //     '<div class="form-group">'+
        //       '<input id="postInput" type="text" class="form-control input-lg post" placeholder="Ketik Status Anda" />' +
        //     '</div>'+
        //       '<button class="postStatus btn btn-info btn-lg btn-block" style="float:left; " onclick="postStatus()">Post ke Facebook</button>' +
              
        //       '</div>'+
        //   '</div>'+
        // '</div>'

        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
        
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed" style="margin-left: 15px; padding-bottom: 10px; padding-top: 10px;">' +
                  '<div class="media">' +
                    '<div class="pull-left">' +
                      '<img class="media-object" src="' + user.picture.data.url + '" alt="Photo">' +
                    '</div>' +
                    '<div class="media-body">' +
                      '<h4 class="media-heading">' + user.name + '</h4>' +
                        '<p>' + value.message + '</p>' +
                        '<p>' + value.story + '</p>' +
                    '</div>' +
                  '</div>' +
                '</div>'
                //   '<ul class="list-group">' +
                //     '<li class="list-group-item">' + value.message + '</li>' +
                //     '<li class="list-group-item">' + value.story + '</li>' +
         
                // '<p>' + value.message + '</p>' +
                //   '<p>' + value.story + '</p>' +
                
                //   '</div>' +
                //    '</div>' 
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed" style="margin-left: 15px; padding-bottom: 10px; padding-top: 10px;">' +
                  '<div class="media">' +
                    '<div class="pull-left">' +
                      '<img class="media-object" src="' + user.picture.data.url + '" alt="Photo">' +
                    '</div>' +
                    '<div class="media-body">' +
                      '<h4 class="media-heading">' + user.name + '</h4>' +
                        '<p>' + value.message + '</p>' +
                    '</div>' +
                  '</div>' +
                '</div>'
                // '<ul class="list-group">' +
                //     '<li class="list-group-item">' + value.message + '</li>' +
                //  '<div class="container style="padding-top: 0px"">' +
                //   '<p>' + value.message + '</p>' +
                // '</div>' +
                // '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed" style="margin-left: 15px; padding-bottom: 10px; padding-top: 10px;">' +
                  '<div class="media">' +
                    '<div class="pull-left">' +
                      '<img class="media-object" src="' + user.picture.data.url + '" alt="Photo">' +
                    '</div>' +
                    '<div class="media-body">' +
                      '<h4 class="media-heading">' + user.name + '</h4>' +
                        '<p>' + value.story + '</p>' +
                    '</div>' +
                  '</div>' +
                '</div>'
                // '<ul class="list-group">' +
                //     '<li class="list-group-item">' + value.story + '</li>' +
                 // '<div class="container style="padding-top: 0px"">' +
                 //  '<p>' + value.story + '</p>' +
                // '</div>' +
                // '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('nav').html(
        '<nav class="navbar navbar-default" role="navigation">' +
          '<div class="container-fluid">' +
           
           '<div class="navbar-header">' +
              '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">' +
                '<span class="sr-only">Toggle navigation</span>' +
                '<span class="icon-bar"></span>' +
                '<span class="icon-bar"></span>' +
               '<span class="icon-bar"></span>' +
              '</button>' +
              '<a class="navbar-brand" href="#">IQBAL</a>' +
            '</div>' +

            '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">' +
              '<ul class="nav navbar-nav">' +
              '</ul>' +
              
            '</div>' +
          '</div>' +
        '</nav>'
      )
      $('#lab8').html('<div class="button" id="lab8">' + 
      '<button class="btn btn-primary" onclick="facebookLogin()">Login with Facebook</button>' + 
      '</div>');
    }
  };


  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
       console.log(response);
        if (response.status === "connected") {
        console.log("You are logged in.");
        render(true);
        
      }
      else if (response.status === "not_authorized") {
        console.log("You are not logged in.");
        
        
      }
      else {

        console.log("You are not logged in yet.");
       
        
      }
     }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})



  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
          render(false);
        }
     });


  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name,cover,picture,gender,about,email', 'GET', function(response){
            console.log(response);
            fun(response);
          });
        }
    });

  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me/posts', 'GET', function(response){
          console.log(response);
          if (response && !response.error) {
            /* handle the result */
            fun(response);
          }
          else {
            swal({
              text: "Something went wrong",
              icon: "error"
            });
          }
        });
      }
  });

  };

  const postFeed = (message) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    
     FB.api('/me/feed', 'POST', {message:message});


  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
    render(true);
  };
